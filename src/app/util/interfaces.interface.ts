export interface LoginResponseInterface {
  token: string;
}

export interface MenuItemInterface {
  title: string;
  icon: string;
  route: string;
}

export interface UsersInterface {
  page: number;
  per_page: number;
  total: number;
  total_pages: 4;
  data: Array<UserInterface>;
}

export interface UserInterface {
  id: number;
  email: string;
  first_name: string;
  last_name: string;
  avatar: string;
}

export interface PostInterface {
  userId: number;
  id: number;
  title: string;
  body: string;
}

export interface AlbumInterface {
  userId: number;
  id: number;
  title: string;
}

export interface PhotoInterface {
  albumId: number;
  id: number;
  title: string;
  url: string;
  thumbnailUrl: string;
}
