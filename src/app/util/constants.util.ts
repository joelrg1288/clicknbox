import {environment} from '../../environments/environment';

export class ConstantsUtil {
  get PHOTOS(): string {
    return this._PHOTOS;
  }

  get ALBUMS(): string {
    return this._ALBUMS;
  }

  get POSTS(): string {
    return this._POSTS;
  }

  get USERS(): string {
    return this._USERS;
  }

  get LOGIN(): string {
    return this._LOGIN;
  }

  private _RE_IP = environment.re;
  private _JT_IP = environment.jt;
  private _RE_API = `${this._RE_IP}api/`;
  private _LOGIN = `${this._RE_API}login/`;
  private _USERS = `${this._RE_API}users/`;
  private _POSTS = `${this._JT_IP}posts/`;
  private _ALBUMS = `${this._JT_IP}users/`;
  private _PHOTOS = `${this._JT_IP}albums/`;
}
