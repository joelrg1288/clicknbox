import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog} from '@angular/material';
import {PostInterface} from '../../util/interfaces.interface';
import {UserService} from '../../services/user.service';
import {ConfirmComponent} from '../confirm/confirm.component';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.scss']
})
export class PostsComponent implements OnInit {

  user = '';
  loading = false;
  posts: Array<PostInterface> = [];

  constructor(@Inject(MAT_DIALOG_DATA) private passedData: any,
              private userService: UserService,
              private matDialog: MatDialog) {
  }

  ngOnInit() {
    if (this.passedData) {
      this.user = this.passedData.user;
      this.posts = this.passedData.posts;
    }
  }

  onDeletePost(id) {
    this.matDialog.open(ConfirmComponent, {
      data: {
        title: '¿Seguro que deseas eliminar?',
        content: 'Si continuas, el post se eliminará permanentemente'
      }
    }).afterClosed().subscribe(result => {
      if (result) {
        this.loading = true;
        this.userService.deletePost(id).subscribe(
          () => {
            const aux = [];
            this.posts.forEach(post => {
              if (post.id !== id) {
                aux.push(post);
              }
            });
            this.posts = aux;
            console.log('Método delete ejecutado');
          },
          error => {
            console.log(error);
          },
          () => {
            this.loading = false;
          }
        );
      }
    });
  }

}
