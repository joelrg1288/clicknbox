import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.scss']
})
export class ConfirmComponent implements OnInit {

  title = '¿Está seguro que desea continuar?';
  content = '';

  constructor(@Inject(MAT_DIALOG_DATA) private passedData: any) {
  }

  ngOnInit() {
    if (this.passedData) {
      this.title = this.passedData.title;
      this.content = this.passedData.content;
    }
  }

}
