import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';
import {PhotoInterface} from '../../util/interfaces.interface';

@Component({
  selector: 'app-photos',
  templateUrl: './photos.component.html',
  styleUrls: ['./photos.component.scss']
})
export class PhotosComponent implements OnInit {

  name = '';
  photos: Array<PhotoInterface> = [];

  constructor(@Inject(MAT_DIALOG_DATA) private passedData: any) {
  }

  ngOnInit() {
    if (this.passedData) {
      this.name = this.passedData.name;
      this.photos = this.passedData.photos;
    }
  }

}
