import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {MaterialModule} from '../material/material.module';
import {FlexLayoutModule} from '@angular/flex-layout';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { ConfirmComponent } from '../elements/confirm/confirm.component';
import { CardComponent } from '../elements/card/card.component';
import { LoaderComponent } from '../elements/loader/loader.component';
import { PostsComponent } from '../elements/posts/posts.component';
import { PhotosComponent } from '../elements/photos/photos.component';


@NgModule({
  declarations: [ConfirmComponent, CardComponent, LoaderComponent, PostsComponent, PhotosComponent],
  imports: [
    CommonModule,
    RouterModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
  ],
  exports: [
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    CardComponent,
    LoaderComponent,
  ],
  entryComponents: [
    ConfirmComponent,
    PostsComponent,
    PhotosComponent
  ]
})
export class SharedModule {
}
