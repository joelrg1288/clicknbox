import {Component, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {Subscription} from 'rxjs';
import {AuthService} from '../../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loading = false;
  loginError = false;
  private subscription: Subscription = new Subscription();

  constructor(private authService: AuthService) {
  }

  ngOnInit() {
    this.subscription.add(this.authService.loginError.subscribe(
      () => {
        this.loginError = true;
      }
    ));
    this.subscription.add(this.authService.loading.subscribe(
      loading => {
        this.loading = loading;
      }
    ));
  }

  onLogin(form: NgForm) {
    this.authService.signInUser({email: form.value.username, password: form.value.password});
  }

}
