import {Component, OnInit} from '@angular/core';
import {AlbumInterface, PhotoInterface, UserInterface, UsersInterface} from '../../util/interfaces.interface';
import {UserService} from '../../services/user.service';
import {MatDialog} from '@angular/material';
import {PhotosComponent} from '../../elements/photos/photos.component';

@Component({
  selector: 'app-album',
  templateUrl: './album.component.html',
  styleUrls: ['./album.component.scss']
})
export class AlbumComponent implements OnInit {

  loading = false;
  users: Array<UserInterface>;
  photos: Array<PhotoInterface> = [];

  constructor(private userService: UserService,
              private matDialog: MatDialog) {
  }

  ngOnInit() {
    this.loading = true;
    this.userService.retrieveUsers(2).subscribe(
      (result: UsersInterface) => {
        this.users = result.data;
      }, error => {
        console.log(error);
      },
      () => {
        this.loading = false;
      }
    );
  }

  onShowAlbum(user: UserInterface) {
    this.photos = [];
    this.loading = true;
    this.userService.retrieveAlbums(user.id).subscribe((result: Array<AlbumInterface>) => {
      const num = Math.round(Math.random() * 93);
      result = result.slice(num, (num + 9));
      result.forEach(i => {
        this.userService.retrievePhotos(i.id).subscribe(
          (res: Array<PhotoInterface>) => {
            const n = Math.round(Math.random() * 5000);
            this.photos.push(res.find(photo => photo.id === n));
          }, error => {
            console.log(error);
          }
        );
      });
    }, error => {
      console.log(error);
    }, () => {
      this.loading = false;
      setTimeout(() => {
        this.matDialog.open(PhotosComponent, {
          data: {
            name: user.first_name + ' ' + user.last_name,
            photos: this.photos
          }
        }).afterClosed();
      }, 1000);
    });
  }

}
