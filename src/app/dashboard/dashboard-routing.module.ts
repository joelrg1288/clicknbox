import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {MainComponent} from './main/main.component';
import {AuthService} from '../services/auth.service';
import {HomeComponent} from './home/home.component';
import {AlbumComponent} from './album/album.component';


const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    canActivateChild: [AuthService],
    children: [
      {
        path: 'home',
        component: HomeComponent
      },
      {
        path: 'album',
        component: AlbumComponent
      },
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'home'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule {
}
