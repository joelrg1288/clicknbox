import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { MainComponent } from './main/main.component';
import { HeaderComponent } from '../navigation/header/header.component';
import { FooterComponent } from '../navigation/footer/footer.component';
import {SharedModule} from '../shared/shared.module';
import { HomeComponent } from './home/home.component';
import { AlbumComponent } from './album/album.component';


@NgModule({
  declarations: [MainComponent, HeaderComponent, FooterComponent, HomeComponent, AlbumComponent],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    SharedModule,
  ]
})
export class DashboardModule { }
