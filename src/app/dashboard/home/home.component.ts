import {Component, OnInit} from '@angular/core';
import {UserService} from '../../services/user.service';
import {PostInterface, UserInterface, UsersInterface} from '../../util/interfaces.interface';
import {NgForm} from '@angular/forms';
import {MatDialog} from '@angular/material';
import {PostsComponent} from '../../elements/posts/posts.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  loading: boolean;
  name: string;
  email: string;
  users: Array<UserInterface>;
  selectedUser: UserInterface = {
    id: 0,
    avatar: '',
    first_name: '',
    last_name: '',
    email: ''
  };
  posts: Array<PostInterface>;

  constructor(private userService: UserService,
              private matDialog: MatDialog) {
  }

  ngOnInit() {
    this.loading = true;
    this.userService.retrieveUsers(1).subscribe(
      (result: UsersInterface) => {
        this.users = result.data;
      }, error => {
        console.log(error);
      },
      () => {
        this.loading = false;
      }
    );
  }

  openSidenav(sidenav, user: UserInterface) {
    if (user === this.selectedUser && sidenav.opened) {
      sidenav.close();
      return;
    }
    this.loading = true;
    this.selectedUser = user;
    this.name = user.first_name + ' ' + user.last_name;
    this.email = user.email;
    this.userService.retrievePosts(user.id).subscribe(
      (result: Array<PostInterface>) => {
        this.posts = result;
      }, result => {
        console.log(result);
      }, () => {
        this.loading = false;
        sidenav.open();
      }
    );
  }

  onEditUser(form: NgForm) {
    this.selectedUser.first_name = form.value.name.split(' ')[0];
    this.selectedUser.last_name = form.value.name.split(' ')[1] ? form.value.name.split(' ')[1] : '';
    this.selectedUser.email = form.value.email;
    this.userService.editUser(this.selectedUser.id, {name: form.value.name, email: form.value.email}).subscribe(
      () => {
        console.log('Se realizó con éxito el método patch');
      }
    );
  }

  onShowPosts() {
    this.matDialog.open(PostsComponent, {
      data: {
        user: this.selectedUser.first_name + ' ' + this.selectedUser.last_name,
        posts: this.posts
      }
    }).afterClosed();
  }

}
