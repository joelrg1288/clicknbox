import {Component, HostListener, OnInit} from '@angular/core';
import {MenuItemInterface} from '../../util/interfaces.interface';
import {MatDialog} from '@angular/material';
import {ConfirmComponent} from '../../elements/confirm/confirm.component';
import {AuthService} from '../../services/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  headerClass = '';
  itemClass = '';

  sections: MenuItemInterface[] = [
    {
      title: 'Home',
      icon: 'home',
      route: 'home'
    },
    {
      title: 'Album',
      icon: 'photo',
      route: 'album'
    }
  ];

  constructor(private matDialog: MatDialog,
              private authService: AuthService) {
  }

  ngOnInit() {
  }

  onLogout() {
    this.matDialog.open(ConfirmComponent, {
      data: {
        title: '¿Está seguro que desea cerrar sesión?',
        content: 'Los datos no guardados se perderán automáticamente'
      }
    }).afterClosed().subscribe(result => {
      if (result) {
        this.authService.logout();
      }
    });
  }

  @HostListener('window:scroll', ['$event'])
  onWindowScroll() {
    if (window.pageYOffset) {
      this.headerClass = 'sticky';
      this.itemClass = 'menu-item-sticky';
    } else {
      this.headerClass = this.itemClass = '';
    }
  }

}
