import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ConstantsUtil} from '../util/constants.util';
import {AuthService} from '../services/auth.service';
import {UserService} from '../services/user.service';



@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [
    ConstantsUtil,
    AuthService,
    UserService,
  ]
})
export class CoreModule { }
