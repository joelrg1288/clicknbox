import {HttpClient, HttpHeaders} from '@angular/common/http';
import {ActivatedRouteSnapshot, CanActivateChild, CanLoad, Route, Router, RouterStateSnapshot} from '@angular/router';
import {Injectable} from '@angular/core';
import {ConstantsUtil} from '../util/constants.util';
import {LoginResponseInterface} from '../util/interfaces.interface';
import {Subject} from 'rxjs';

@Injectable()
export class AuthService implements CanLoad, CanActivateChild {

  constructor(private router: Router,
              private httpClient: HttpClient,
              private constantsUtil: ConstantsUtil) {
  }

  loading = new Subject<any>();
  loginError = new Subject<any>();

  static onSaveUserData(data) {
    localStorage.setItem('user', JSON.stringify(data));
  }

  static onLoadUserData() {
    return JSON.parse(localStorage.getItem('user'));
  }

  static getToken() {
    return AuthService.onLoadUserData() ? AuthService.onLoadUserData().token : null;
  }

  static getGlobalHeaders() {
    return new HttpHeaders({
      Authorization: 'Bearer ' + AuthService.getToken()
    });
  }

  static isAuthenticated() {
    return AuthService.getToken() != null;
  }

  signInUser(data) {
    this.loading.next(true);
    this.httpClient.post(this.constantsUtil.LOGIN, data)
      .subscribe(
        (res: LoginResponseInterface) => {
          AuthService.onSaveUserData({token: res.token});
          this.router.navigate(['/clicknbox']).then();
        },
        error => {
          console.log(error);
          this.loginError.next(true);
          this.logout();
        },
        () => {
          this.loading.next(false);
        }
      );
  }

  canLoad(route: Route): boolean {
    if (!AuthService.isAuthenticated()) {
      this.logout();
      return false;
    }
    return true;
  }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (!AuthService.isAuthenticated()) {
      this.router.navigate(['/auth']).then();
      return false;
    }
    return true;
  }

  logout() {
    localStorage.clear();
    this.router.navigate(['/auth']).then();
  }
}

