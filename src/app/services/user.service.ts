import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ConstantsUtil} from '../util/constants.util';
import {AuthService} from './auth.service';
import {UsersInterface} from '../util/interfaces.interface';
import {Subject} from 'rxjs';

@Injectable()
export class UserService {

  constructor(private httpClient: HttpClient,
              private constantsUtil: ConstantsUtil) {
  }

  retrieveUsers(page) {
    return this.httpClient.get(`${this.constantsUtil.USERS}?page=${page}`, {headers: AuthService.getGlobalHeaders()});
  }

  editUser(id, data) {
    return this.httpClient.patch(`${this.constantsUtil.USERS}${id}`, data, {headers: AuthService.getGlobalHeaders()});
  }

  retrievePosts(id) {
    return this.httpClient.get(`${this.constantsUtil.POSTS}?userId=${id}`, {headers: AuthService.getGlobalHeaders()});
  }

  deletePost(id) {
    return this.httpClient.delete(`${this.constantsUtil.POSTS}${id}`, {headers: AuthService.getGlobalHeaders()});
  }

  retrieveAlbums(id) {
    return this.httpClient.get(`${this.constantsUtil.ALBUMS}${id}/albums`, {headers: AuthService.getGlobalHeaders()});
  }

  retrievePhotos(id) {
    return this.httpClient.get(`${this.constantsUtil.PHOTOS}${id}/photos`, {headers: AuthService.getGlobalHeaders()});
  }
}
