import {NgModule} from '@angular/core';
import {Routes, RouterModule, PreloadAllModules} from '@angular/router';
import {AuthService} from './services/auth.service';


const routes: Routes = [
  {
    path: 'auth',
    loadChildren: './auth/auth.module#AuthModule'
  },
  {
    path: 'clicknbox',
    loadChildren: './dashboard/dashboard.module#DashboardModule',
    canLoad: [AuthService]
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'clicknbox'
  },
  {
    path: '**',
    redirectTo: 'auth'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules})],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
